import React from 'react';
import image from './topimg.png';
import './img.css';

const Img = () => {
    return (
        <div className='img'>
            <img src={image} alt=''></img>
        </div>
    )
}

export default Img;