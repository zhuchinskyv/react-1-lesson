import React from 'react';
import NavBar from '../nav-bar';
import Img from '../img';
import Title from '../title';
import Text from '../text';
import Button from '../button';
import Foot from '../foot';


import './app.css';

const App = () => {
    return (
        <div className='container'>
            <NavBar></NavBar>
            <Img></Img>
            <Title></Title>
            <Text></Text>
            <Button></Button>
            <Foot></Foot>
        </div>
    )
}


export default App;