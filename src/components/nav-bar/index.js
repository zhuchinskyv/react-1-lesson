import React from 'react';

import './nav-bar.css';


const NavBar = () => {
    return (
        <div className='navbar'>
            <ul>
                <li>Home</li>
                <li>PhotoApp</li>
                <li>Design</li>
                <li>Download</li>
            </ul>
        </div>
    )
}


export default NavBar;