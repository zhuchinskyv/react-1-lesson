import React from "react";

import './text.css';

const Text = () => {
    return (
        <div className='text'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, eveniet rem qui dolorem adipisci repudiandae esse? Ducimus repellat provident a error, nulla quam illum asperiores at exercitationem aliquid dolores laboriosam.
            Omnis eum tempore sapiente earum, dignissimos quam quae recusandae quos quasi molestias doloremque perferendis repellendus iste! Doloremque vel amet asperiores optio sapiente consectetur, consequatur rerum, voluptatum enim error reiciendis at.
            Voluptates sapiente consequatur iste repellat odit quam ratione necessitatibus pariatur dicta ad ullam dolores similique est alias iure cumque nesciunt, soluta autem voluptas! Ea suscipit omnis aperiam saepe. Ratione, ad.
            Eum eius accusantium saepe blanditiis, quidem laudantium obcaecati aliquid? Expedita eaque minus soluta totam dolorem alias voluptatum temporibus consectetur ducimus, earum, ut eveniet nemo! Necessitatibus officia possimus aperiam dolore! Provident!
            Saepe asperiores debitis omnis voluptatibus magni quo hic obcaecati quaerat assumenda quos reprehenderit, perspiciatis perferendis nemo similique architecto sequi. Laborum atque nobis aperiam! Explicabo totam voluptas praesentium facere architecto asperiores!
            Asperiores dolorum est quidem voluptatibus beatae possimus excepturi? Quae labore id reprehenderit ex molestiae neque nobis quisquam! Sunt, eos ducimus maiores enim rem suscipit eaque maxime dolorum officiis ad sit!
            <div className='text-two'>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, nobis necessitatibus facilis obcaecati autem, dolore labore maiores, voluptas praesentium repellendus dicta suscipit mollitia. Ad quas hic quasi error deserunt accusantium.
                Itaque ipsa ex, molestiae accusantium voluptate optio quod, aliquam cupiditate, a aut similique iste quia necessitatibus ducimus ratione! Dolorem atque facere quis voluptates vero quidem quod eos tenetur iste minima.
                Rerum cupiditate porro, numquam pariatur ad tenetur qui praesentium. Quisquam, doloribus? Numquam natus reiciendis quas quis quo doloribus tempore eveniet ratione nulla nobis, voluptatibus aperiam cumque ullam temporibus totam deleniti!
                Qui similique non sed labore eius laboriosam fugit rerum, culpa nesciunt iste ducimus accusamus ab, tempora alias, sapiente cupiditate? Porro asperiores, praesentium sit illum recusandae nam similique beatae aut excepturi!
                Reiciendis ut quis error exercitationem hic tempore facilis sapiente incidunt amet adipisci. Quod eveniet illo ipsa dicta incidunt, fuga, distinctio facilis explicabo saepe deleniti unde, ad enim ex doloremque quo!
            </div>
        </div>
    )
}

export default Text;